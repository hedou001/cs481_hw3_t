﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_T.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Profile : ContentPage
    {
        public Profile()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Here we can fetch a CSUSM API to get the user data
            // userData = CsusmApi.FetchUserProfile();
            // FirstNameLabel.Text = userData.FirstName;
            // ...
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}