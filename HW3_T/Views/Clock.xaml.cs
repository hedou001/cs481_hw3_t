﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_T.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Clock : ContentPage
    {
        private System.Timers.Timer timer = new System.Timers.Timer();
        private int seconds = 0;
        public Clock()
        {
            InitializeComponent();

            // Setup and starting the timer
            timer.Start();
            timer.Interval = 1000;
            // Instantly pausing the timer (so we can start it when the page is appearing on screen)
            timer.Enabled = false;
            // Setting the callback of the timer
            timer.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) =>
            {
                seconds += 1;
                TimeSpan time = TimeSpan.FromSeconds(seconds);
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {
                    TimerDisplay.Text = "" + time.ToString(@"hh\:mm\:ss");
                });
            };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            // Starting timer
            timer.Enabled = true;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            // Stopping timer
            timer.Enabled = false;
        }
    }
}