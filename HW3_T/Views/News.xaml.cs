﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_T.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class News : ContentPage
    {
        private double scrollPos = 0.0f;
        public News()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Resetting the scroll position
            Scroller.ScrollToAsync(0.0f, 0.0f, false);
            // Restoring the scroll position
            Scroller.ScrollToAsync(0.0f, scrollPos, true);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            // Saving the scroll progress so we can restore it
            scrollPos = Scroller.ScrollY;
        }
    }
}